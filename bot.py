import datetime
import discord
from discord.ext import commands, tasks
import os
import json
from pymongo import MongoClient
import time
import asyncio

cluster = MongoClient(
    "mongodb://172.105.15.252:27017"
)
StudyStreamDB = cluster["StudyStreamDB"]
StudyStreamCol = StudyStreamDB["StudyStream"]


async def dm(member, message):
    await member.create_dm()
    await member.send(f"{message}")


async def bantimer(time, member):
    ban = time * 60
    while ban > 0:
        await asyncio.sleep(60)
        StudyStreamCol.update_one({"vc-ban": {"$exists": True}}, {"$inc": {
            f"vc-ban.{member}": -1
        }})
        ban -= 1


@tasks.loop(seconds=5)
async def monitorvc(ctx):
    users_kicked = 0
    channels = bot.get_all_channels()
    for channel in channels:
        if channel.type == discord.ChannelType.voice:
            for member in channel.members:
                video = member.voice.self_video
                blacklist = StudyStreamCol.find_one({"blacklists": {"$exists": True}})
                isblacklisted = False
                if blacklist is None:
                    pass
                else:
                    blkchannel = blacklist["blacklists"][f"{member.voice.channel.name}"]
                    if blkchannel is None:
                        pass
                    elif blkchannel is False:
                        pass
                    elif blkchannel is True:
                        isblacklisted = True

                if not video and not member.bot and not isblacklisted:
                    users_kicked += 1
                    await dm(member,
                             "Hey, we noticed your camera is off, please enable it to use this StudyStream VC, otherwise you will be kicked from the channel shortly.")
                    await asyncio.sleep(60)
                    await member.move_to(None)
                    vc_bans = StudyStreamCol.find_one({"vc-ban": {"$exists": True}})
                    ban = vc_bans["vc-ban"][f"{member}"]
                    if ban is None:
                        StudyStreamCol.update_one({f"vc-ban": {"$exists": True}}, {"$set": {
                            f"vc-ban.{member}": 0
                        }})
                        ban = ban["vc-ban"][f"{member}"]
                    if ban == 0 or ban == 1:
                        StudyStreamCol.update_one({"vc-ban": {"$exists": True}}, {"$inc": {
                            f"vc-ban.{member}": 1
                        }})
                    elif ban == 2:
                        StudyStreamCol.update_one({f"vc-ban": {"$exists": True}}, {"$set": {
                            f"vc-ban.{member}": 5}
                        })
                        await bantimer(5, member)
                        await dm(member,
                                 f"You joined a camera-required StudyStream VC too many times without a camera on, you are now banned from joining again for {str(5)} minutes. \nPlease contact @!825321720835604490 if you have any questions about this VC ban.")


bot = commands.Bot(command_prefix=",")


@bot.event
async def on_ready():
    StudyStreamCol.delete_one({"vc-ban": {"$exists": True}})
    StudyStreamCol.insert_one({"vc-ban": {}})
    print("Online")


@bot.event
async def on_voice_state_update(member, before, after):
    try:
        banDoc = StudyStreamCol.find_one({"vc-ban": {"$exists": True}})
        blacklist = StudyStreamCol.find_one({"blacklists": {"$exists": True}})
    except TypeError:
        pass
    if banDoc["vc-ban"] == {}:
        StudyStreamCol.update_one({"vc-ban": {"$exists": True}}, {"$set": {
            f"vc-ban.{member}": 0
        }})
    vc_bans = StudyStreamCol.find_one({"vc-ban": {"$exists": True}})
    ban = vc_bans["vc-ban"][f"{member}"]

    if member.voice is None:
        pass
    else:
        try:
            userchannel = member.voice.channel.name
            blkchannel = blacklist["blacklists"][f"{member.voice.channel.name}"]
        except KeyError:
            pass

        if blkchannel is None:
            pass
        if blacklist is None or blkchannel == False or blkchannel is None:
            if ban == 0 or ban == 1 or ban == 2:
                pass
            else:
                await member.move_to(None)
        else:
            pass


@bot.command()
@commands.has_role("Admin")
async def ping(ctx):
    await ctx.send(f"Ping {round(bot.latency * 1000)}ms")


@bot.command()
@commands.has_role("Admin")
async def join(ctx):
    author_vc = ctx.author.voice
    if author_vc:
        await author_vc.channel.connect()


@bot.command()
@commands.has_role("Admin")
async def leave(ctx):
    await ctx.voice_client.disconnect()


@bot.command(aliases=["voice"])
@commands.has_role("Admin")
async def vc(ctx, detail, member: discord.Member):
    if member.voice:
        if detail == "details":
            embed = discord.Embed(
                title=f"{member}'s VC Details",
                colour=discord.Colour.blue(),
                description=f"VC Details"
            )
            embed.add_field(name="Muted", value=f"{member.voice.self_mute}", inline=True)
            embed.add_field(name="Deafend", value=f"{member.voice.self_deaf}", inline=True)
            embed.add_field(name="Video", value=f"{member.voice.self_video}", inline=True)
            embed.add_field(name="Streaming", value=f"{member.voice.self_stream}", inline=True)
            embed.add_field(name="Suppressed", value=f"{member.voice.suppress}", inline=True)
            embed.add_field(name="AFK", value=f"{member.voice.afk}", inline=True)
            embed.add_field(name="Channel", value=f"{member.voice.channel}", inline=True)
            embed.set_thumbnail(url=f"{member.avatar_url}")

        elif detail == "video" or detail == "camera":
            embed = discord.Embed(
                title=f"{member}'s VC Details",
                colour=discord.Colour.blue(),
                description=f"VC Video"
            )
            embed.add_field(name="Video", value=f"{member.voice.self_video}", inline=True)
            embed.set_thumbnail(url=f"{member.avatar_url}")

        elif detail == "mute" or detail == "muted":
            embed = discord.Embed(
                title=f"{member}'s VC Details",
                colour=discord.Colour.blue(),
                description=f"VC Muted"
            )
            embed.add_field(name="Muted", value=f"{member.voice.self_mute}", inline=True)
            embed.set_thumbnail(url=f"{member.avatar_url}")

        elif detail == "deaf" or detail == "deafened":
            embed = discord.Embed(
                title=f"{member}'s VC Details",
                colour=discord.Colour.blue(),
                description=f"VC Deafened"
            )
            embed.add_field(name="Deafened", value=f"{member.voice.self_deaf}", inline=True)
            embed.set_thumbnail(url=f"{member.avatar_url}")

        elif detail == "stream" or detail == "streaming":
            embed = discord.Embed(
                title=f"{member}'s VC Details",
                colour=discord.Colour.blue(),
                description=f"VC Stream"
            )
            embed.add_field(name="Streaming", value=f"{member.voice.self_stream}", inline=True)
            embed.set_thumbnail(url=f"{member.avatar_url}")

        elif detail == "suppress" or detail == "suppressed":
            embed = discord.Embed(
                title=f"{member}'s VC Details",
                colour=discord.Colour.blue(),
                description=f"VC Suppressed"
            )

            embed.add_field(name="Suppressed", value=f"{member.voice.suppress}", inline=True)
            embed.set_thumbnail(url=f"{member.avatar_url}")

        elif detail == "afk" or detail == "AFK":
            embed = discord.Embed(
                title=f"{member}'s VC Details",
                colour=discord.Colour.blue(),
                description=f"VC AFK"
            )

            embed.add_field(name="AFK", value=f"{member.voice.afk}", inline=True)
            embed.set_thumbnail(url=f"{member.avatar_url}")

        elif detail == "channel":
            embed = discord.Embed(
                title=f"{member}'s VC Details",
                colour=discord.Colour.blue(),
                description=f"VC Channel"
            )

            embed.add_field(name="Channel", value=f"{member.voice.channel}", inline=True)
            embed.set_thumbnail(url=f"{member.avatar_url}")

        else:
            await ctx.send("Please Specify Valid Detail")
    else:
        await ctx.send("User is not in VC")

    embed.set_footer(text=f"{ctx.message.author}", icon_url=f"{ctx.message.author.avatar_url}")
    await ctx.send(embed=embed)


async def vcerror(ctx, error):
    embed = discord.Embed(
        title=f"Error",
        colour=discord.Colour.red(),
        description=f"{error}"
    )
    embed.set_footer(text=f"{ctx.message.author}", icon_url=f"{ctx.message.author.avatar_url}")
    embed.add_field(name="Format:", value="```vc [detail] [user]```", inline=True)
    await ctx.send(embed=embed)


@bot.command(aliases=["mod"])
@commands.has_role("Admin")
async def monitor(ctx, type, state):
    if type == "vc" or type == "voice":

        if state == "start" or state == "enable":
            embed = discord.Embed(
                title="VC Moderating",
                colour=discord.Colour.blue(),
                description="VC Monitoring/Moderating is on"
            )
            embed.add_field(name="Status:", value="Enabled", inline=True)
            embed.set_thumbnail(
                url=f"https://cdn.discordapp.com/avatars/840292306171068428/c19da07ecc6201b32c1ee14abf6b546b.webp?size=1024")
            embed.set_footer(text=f"{ctx.message.author}", icon_url=f"{ctx.message.author.avatar_url}")

            await ctx.send(embed=embed)
            monitorvc.start(ctx)

        elif state == "stop" or state == "disable":
            embed = discord.Embed(
                title="VC Moderating",
                colour=discord.Colour.blue(),
                description="VC Monitoring/Moderating is off"
            )
            embed.add_field(name="Status:", value="Disabled", inline=True)
            embed.set_thumbnail(
                url=f"https://cdn.discordapp.com/avatars/840292306171068428/c19da07ecc6201b32c1ee14abf6b546b.webp?size=1024")
            embed.set_footer(text=f"{ctx.message.author}", icon_url=f"{ctx.message.author.avatar_url}")

            await ctx.send(embed=embed)
            monitorvc.cancel()

        else:
            await ctx.send("Please Specify (start or stop)")


@monitor.error
async def monitorerror(ctx, error):
    embed = discord.Embed(
        title="Error:",
        colour=discord.Colour.red(),
        description=f"{error}"
    )
    await ctx.send(embed=embed)


@bot.command(aliases=["credits"])
async def credit(ctx):
    embed = discord.Embed(
        title=f"",
        colour=discord.Colour.blue(),
        description=f"Bot Developer"
    )
    embed.set_author(name="Sangster#1348",
                     icon_url="https://cdn.discordapp.com/avatars/714945050265518163/d18b7821b3c201d0667cffa54c1acaac.webp?size=1024")
    embed.set_thumbnail(
        url="https://cdn.discordapp.com/avatars/714945050265518163/d18b7821b3c201d0667cffa54c1acaac.webp?size=1024")
    await ctx.send(embed=embed)


@bot.command()
@commands.has_role("Admin")
async def avatar(ctx, member: discord.Member):
    await ctx.send(member.avatar_url)


@avatar.error
async def avatarError(ctx, error):
    embed = discord.Embed(
        title="Error:",
        colour=discord.Colour.red(),
        description=f"{error}"
    )
    await ctx.send(embed=embed)


@bot.command(aliases=["vccount", "voice-count", "vc-count"])
@commands.has_role("Admin")
async def vc_count(ctx):
    channels = bot.get_all_channels()
    vc_user_count = 0
    for channel in channels:
        if channel.type == discord.ChannelType.voice:
            for user in channel.members:
                vc_user_count += 1

    embed = discord.Embed(
        title="VC Count",
        colour=discord.Colour.blue(),
        description=""
    )
    embed.set_thumbnail(
        url=f"https://cdn.discordapp.com/avatars/840292306171068428/c19da07ecc6201b32c1ee14abf6b546b.webp?size=1024")
    embed.add_field(name="Current VC Count:", value=f"{str(vc_user_count)}", inline=True)
    embed.set_footer(text=f"{ctx.message.author}", icon_url=f"{ctx.message.author.avatar_url}")
    await ctx.send(embed=embed)
    x = StudyStreamCol.find_one({"Server-VC-Count": {"$exists": True}}, {"_id": 0})
    if x is None:
        StudyStreamCol.insert_one({"Server-VC-Count": vc_user_count})
    StudyStreamCol.update_one({"Server-VC-Count": {"$exists": True}}, {"$set": {"Server-VC-Count": vc_user_count}})


@vc_count.error
async def vc_counterror(ctx, error):
    embed = discord.Embed(
        title="Error:",
        colour=discord.Colour.red(),
        description=f"{error}"
    )
    await ctx.send(embed=embed)


@tasks.loop(seconds=1)
async def trackvcusers():
    channels = bot.get_all_channels()
    vc_user_count = 0
    for channel in channels:
        if channel.type == discord.ChannelType.voice:
            for user in channel.members:
                vc_user_count += 1

    x = StudyStreamCol.find_one({"Server-VC-Count": {"$exists": True}}, {"_id": 0})
    if x is None:
        StudyStreamCol.insert_one({"Server-VC-Count": vc_user_count})
    StudyStreamCol.update_one({"Server-VC-Count": {"$exists": True}}, {"$set": {"Server-VC-Count": vc_user_count}})


@bot.command()
@commands.has_role("Admin")
async def track(ctx, data, state):
    if data == "current-vc" and state == "start" or state == "enable":
        trackvcusers.start()
        embed = discord.Embed(
            title="Live VC Data:",
            colour=discord.Colour.blue(),
            description="Status Enabled"
        )
        embed.set_footer(text=f"{ctx.message.author}", icon_url=f"{ctx.message.author.avatar_url}")
        embed.set_thumbnail(
            url=f"https://cdn.discordapp.com/avatars/840292306171068428/c19da07ecc6201b32c1ee14abf6b546b.webp?size=1024")
        await ctx.send(embed=embed)
    elif data == "current-vc" and state == "stop" or state == "disable":
        trackvcusers.cancel()
        embed = discord.Embed(
            title="Live VC Data:",
            colour=discord.Colour.blue(),
            description="Status Disabled"
        )
        embed.set_footer(text=f"{ctx.message.author}", icon_url=f"{ctx.message.author.avatar_url}")
        embed.set_thumbnail(
            url=f"https://cdn.discordapp.com/avatars/840292306171068428/c19da07ecc6201b32c1ee14abf6b546b.webp?size=1024")
        await ctx.send(embed=embed)


@track.error
async def trackerror(ctx, error):
    embed = discord.Embed(
        title="Error:",
        colour=discord.Colour.red(),
        description=f"{error}"
    )
    await ctx.send(embed=embed)


@bot.command()
@commands.has_role("Admin")
async def blacklist(ctx):
    try:
        channel = ctx.message.author.voice.channel
        blacklists = StudyStreamCol.find_one({"blacklists": {"$exists": True}})
        if blacklists is None:
            StudyStreamCol.insert_one({"blacklists": {

            }})
        channels = bot.get_all_channels()
        for c in channels:
            if c.type == discord.ChannelType.voice:
                StudyStreamCol.update_one({"blacklists": {"$exists": True}}, {"$set": {
                    f"blacklists.{c}": None
                }})

        StudyStreamCol.update_one({"blacklists": {"$exists": True}}, {"$set": {
            f"blacklists.{channel}": True
        }})

        await ctx.send(f"{channel} is now blacklisted from VC Mod")

    except AttributeError:
        await ctx.send("You must be in VC to blacklist it")


@blacklist.error
async def blacklisterror(ctx, error):
    embed = discord.Embed(
        title="Error:",
        colour=discord.Colour.red(),
        description=f"{error}"
    )
    await ctx.send(embed=embed)


@bot.command()
@commands.has_role("Admin")
async def whitelist(ctx):
    try:
        channel = ctx.message.author.voice.channel
        blacklists = StudyStreamCol.find_one({"blacklists": {"$exists": True}})
        if blacklists is None:
            StudyStreamCol.insert_one({"blacklists": {

            }})
        channels = bot.get_all_channels()
        for c in channels:
            if c.type == discord.ChannelType.voice:
                StudyStreamCol.update_one({"blacklists": {"$exists": True}}, {"$set": {
                    f"blacklists.{c}": None
                }})

        StudyStreamCol.update_one({"blacklists": {"$exists": True}}, {"$set": {
            f"blacklists.{channel}": False
        }})

        await ctx.send(f"{channel} is now whitelisted from VC Mod")
    except AttributeError:
        await ctx.send("You must be in VC to whitelist it")


@whitelist.error
async def whitelisterror(ctx, error):
    embed = discord.Embed(
        title="Error:",
        colour=discord.Colour.red(),
        description=f"{error}"
    )
    await ctx.send(embed=embed)


bot.run("ODQwMjkyMzA2MTcxMDY4NDI4.YJWFMQ.3xXb5AfzjkZqIty5-ucq3bWv80A")